extern crate strum;
#[macro_use]
extern crate strum_macros;
extern crate num_derive;
extern crate num_traits;

mod grid;
mod gui;

fn main() {
    gui::gui::gui();
}
