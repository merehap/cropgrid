use std::collections::hash_map::DefaultHasher;
use std::fs::File;
use std::hash::{Hash, Hasher};
use std::io::{Read, Write};
use std::num::NonZeroU8;

use fltk::{prelude::*, *};

use crate::grid::grid::Grid;
use crate::grid::square::{CropState, GrowingStatus};
use crate::grid::location::Location;
use crate::grid::crop::Crop;
use crate::grid::placement::Placement;
use std::cell::RefCell;
use std::rc::Rc;

#[derive(Clone, Copy)]
enum Message {
    Set,
    Delete,
}

pub fn gui() {
    let default_width: NonZeroU8 = NonZeroU8::new(20)
        .expect("Width must be positive.");
    let default_height: NonZeroU8 = NonZeroU8::new(9)
        .expect("Height must be positive.");

    let grid = if let Ok(mut file) = File::open("data/grid") {
        let mut contents = String::new();
        file.read_to_string(&mut contents).unwrap();
        Grid::deserialize(&contents.trim())
    } else {
        Grid::new(default_width, default_height)
    };

    let app = app::App::default().with_scheme(app::Scheme::Gtk);
    let mut wind = window::Window::default().with_size(1600, 600);
    let mut table = table::Table::default()
        .with_size(1600 - 10, 600 - 10)
        .center_of(&wind);

    table.set_rows(grid.height().get() as i32);
    table.set_row_header(true);
    table.set_row_resize(true);
    table.set_cols(grid.width().get() as i32);
    table.set_col_header(true);
    table.set_col_width_all(80);
    table.set_col_resize(true);
    table.end();

    let mut choice = menu::Choice::new(100, 500, 200, 40, "Crop");
    for crop in Crop::all() {
        choice.add_choice(&crop.to_string());
    }

    choice.set_value(0);
    choice.set_color(enums::Color::White);

    let mut set_button = button::Button::new(300, 500, 80, 40, "Set Crop");
    let mut delete_button = button::Button::new(400, 500, 80, 40, "Delete Crop");

    wind.make_resizable(true);
    wind.end();
    wind.show();

    let (sender, receiver) = app::channel::<Message>();
    let grid_rc = Rc::new(RefCell::new(grid));

    // Called when the table is drawn then when it's redrawn due to events
    let grid_draw = grid_rc.clone();
    table.draw_cell(move |t, ctx, row, col, x, y, w, h| match ctx {
        table::TableContext::StartPage => draw::set_font(enums::Font::Helvetica, 14),
        table::TableContext::ColHeader => {
            draw_header(&format!("{}", (col + 65) as u8 as char), x, y, w, h)
        } // Column titles
        table::TableContext::RowHeader => draw_header(&format!("{}", row + 1), x, y, w, h), // Row titles
        table::TableContext::Cell => draw_data(
            grid_draw.borrow().at(Location {column: col as u8, row: row as u8}).unwrap().crop(),
            x,
            y,
            w,
            h,
            t.is_selected(row, col),
        ), // Data in cells
        _ => (),
    });

    set_button.emit(sender, Message::Set);
    delete_button.emit(sender, Message::Delete);

    while app.wait() {
        if let Some(val) = receiver.recv() {
            let (row_top, col_left, row_bot, col_right) = table.get_selection();
            if row_top == -1 || col_left == -1 || row_bot == -1 || col_right == -1 {
                continue;
            }

            let placement = Placement {
                left_column: col_left as u8,
                top_row: row_top as u8,
                width: NonZeroU8::new((col_right - col_left + 1) as u8).unwrap(),
                height: NonZeroU8::new((row_bot - row_top + 1) as u8).unwrap(),
            };
            match val {
                Message::Set => {
                    grid_rc.clone().borrow_mut().place_crop(
                        placement, 
                        CropState {
                            crop: Crop::all()[choice.value() as usize],
                            status: GrowingStatus::Planting
                        },
                    ).unwrap();
                },
                Message::Delete => {
                    grid_rc.clone().borrow_mut().delete_placement(placement).unwrap();
                },
            }

            let mut file = File::create("data/grid").unwrap();
            file.write_all(grid_rc.clone().borrow().serialize().as_bytes()).unwrap();

            wind.redraw();
            // sleeps are necessary when calling redraw in the event loop
            app::sleep(0.016);
        }
    }
}

fn draw_header(txt: &str, x: i32, y: i32, w: i32, h: i32) {
    draw::push_clip(x, y, w, h);
    draw::draw_box(
        enums::FrameType::ThinUpBox,
        x,
        y,
        w,
        h,
        enums::Color::FrameDefault,
    );
    draw::set_draw_color(enums::Color::Black);
    draw::draw_text2(txt, x, y, w, h, enums::Align::Center);
    draw::pop_clip();
}

// The selected flag sets the color of the cell to a grayish color, otherwise white
fn draw_data(crop: Option<Crop>, x: i32, y: i32, w: i32, h: i32, selected: bool) {
    draw::push_clip(x, y, w, h);

    let text;
    let mut color;
    if let Some(crop) = crop {
        text = crop.to_string();

        let mut s = DefaultHasher::new();
        crop.hash(&mut s);
        let hash = s.finish();
        color = (hash & 0x00FF_FFFF) as u32;
    } else {
        text = "(Empty)".to_string();
        color = 0x00FF_FFFF;
    }

    if selected {
        // Invert the color.
        color ^= 0x00FFFFFF;
    }

    draw::set_draw_color(enums::Color::from_u32(color));

    draw::draw_rectf(x, y, w, h);
    draw::set_draw_color(enums::Color::Gray0);
    draw::draw_text2(&text, x, y, w, h, enums::Align::Center);
    draw::draw_rect(x, y, w, h);
    draw::pop_clip();
}