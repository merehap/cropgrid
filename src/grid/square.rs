use crate::grid::crop::Crop;

#[derive(Clone, Copy)]
pub struct Square {
    pub soil: Soil,
    crop_state: Option<CropState>,
}

impl Square {
    pub fn empty(soil: Soil) -> Square {
        Square {
            soil,
            crop_state: None,
        }
    }

    pub fn has_crop(&self) -> bool {
        self.crop_state.is_some()
    }

    pub fn crop(&self) -> Option<Crop> {
        self.crop_state.map(|cs| cs.crop)
    }

    pub fn crop_state(&mut self) -> Option<CropState> {
        self.crop_state
    }

    pub fn set_crop_state(&mut self, crop_state: CropState) {
        self.crop_state = Some(crop_state);
    }

    pub fn clear_crop_state(&mut self) {
        self.crop_state = None;
    }
}

#[derive(Clone, Copy)]
pub struct Soil {
    pub soil_type: SoilType,
    pub ph: PH,
}

impl Soil {
    pub fn neutral_loam() -> Soil {
        Soil {
            soil_type: SoilType::Loam,
            ph: PH::neutral(),
        }
    }
}

#[derive(Clone, Copy, PartialEq, PartialOrd)]
pub struct PH {
    ph: f32,
}

impl PH {
    pub fn new(ph: f32) -> PH {
        PH {ph}
    }

    pub fn neutral() -> PH {
        PH::new(7.0)
    }

    pub fn is_in_range(&self, low: PH, high: PH) -> bool {
        low <= *self && *self <= high
    }
}

#[derive(Clone, Copy)]
pub enum SoilType {
    Loam,

    Clay,
    Sand,
    Silt,

    ClayLoam,
    SandyLoam,
    SiltLoam,

    SandyClayLoam,
    SiltyClayLoam,

    SandyClay,
    LoamySand,
    SiltyClay,
}

#[derive(Copy, Clone)]
pub struct CropState {
    pub crop: Crop,
    pub status: GrowingStatus,
}

#[derive(Copy, Clone)]
pub enum GrowingStatus {
    Planting,
    Growing,
    Harvesting,
}

