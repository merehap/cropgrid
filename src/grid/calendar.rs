use std::num::NonZeroU8;
use datetime::LocalDate;

use crate::grid::grid::Grid;
use crate::grid::interval::Interval;
use crate::grid::square::CropState;

pub struct Calendar {
    intervals: Vec<Interval>,
    grid_width: NonZeroU8,
    grid_height: NonZeroU8,
}

impl Calendar {
    pub fn grid_on_date(&self, date: &LocalDate) -> Grid {
        let mut grid = Grid::new(self.grid_width, self.grid_height);
        for interval in &self.intervals {
            if let Some(status) = interval.growing_status_on_date(date) {
                let crop_state = CropState {
                    crop: interval.crop,
                    status
                };
                grid.place_crop(interval.placement.clone(), crop_state)
                    .unwrap_or_else(|_| panic!("Failed to build grid on date {:?}", date));
            }
        }

        grid
    }

    pub fn intervals_on_date(&self, date: &LocalDate) -> Vec<Interval> {
        self.intervals.iter()
            .filter(|interval| interval.growing_status_on_date(date).is_some())
            .cloned()
            .collect()
    }
}
