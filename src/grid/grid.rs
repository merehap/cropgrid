use std::fmt::Write;
use std::num::NonZeroU8;
use std::str::FromStr;
use std::string::ToString;

use crate::grid::crop::Crop;
use crate::grid::location::Location;
use crate::grid::placement::Placement;
use crate::grid::square::{Square, Soil, GrowingStatus, CropState};

pub struct Grid {
    grid: Vec<Vec<Square>>,
}

impl Grid {
    pub fn new(width: NonZeroU8, height: NonZeroU8) -> Grid {
        let mut grid = Vec::new();
        for _ in 0..height.get() {
            let square = Square::empty(Soil::neutral_loam());
            grid.push(vec![square; width.get() as usize]);
        }

        Grid {grid}
    }

    pub fn width(&self) -> NonZeroU8 {
        NonZeroU8::new(self.grid[0].len() as u8)
            .expect("Width must be positive.")
    }

    pub fn height(&self) -> NonZeroU8 {
        NonZeroU8::new(self.grid.len() as u8)
            .expect("Length must be positive.")
    }

    pub fn is_in_bounds(&self, location: Location) -> bool {
        location.column < self.width().get() && location.row < self.height().get()
    }

    pub fn at(&self, location: Location) -> Result<Square, String> {
        if !self.is_in_bounds(location) {
            return Err(format!("({},{}) is out of bounds.", location.column, location.row));
        }

        Ok(self.grid[location.row as usize][location.column as usize])
    }

    pub fn place_crop(
        &mut self,
        placement: Placement,
        crop_state: CropState,
    ) -> Result<(), String> {

        if !self.is_in_bounds(placement.bottom_right()) {
            return Err(format!(
                "Failed to delete placement. Bottom right location ({:?}) exceeds width or height.",
                placement.bottom_right()
            ));
        }

        for location in placement.locations() {
            self.square_at_mut(location).set_crop_state(crop_state);
        }

        Ok(())
    }

    pub fn delete_placement(
        &mut self,
        placement: Placement,
    ) -> Result<(), String> {
        if !self.is_in_bounds(placement.bottom_right()) {
            return Err(format!(
                "Failed to delete placement. Bottom right location ({:?}) exceeds width or height.",
                placement.bottom_right()
            ));
        }

        for location in placement.locations() {
            self.square_at_mut(location).clear_crop_state();
        }

        Ok(())
    }

    pub fn total_calories(&self) -> usize {
        let mut calories = 0;
        for crop in self.crops() {
            calories += 100 * crop.crop_info().calories / 365;
        }

        calories
    }

    pub fn serialize(&self) -> String {
        let mut text = "".to_string();
        writeln!(text, "{} {}", self.width(), self.height()).unwrap();
        for column in 0..self.width().get() as usize {
            for row in 0..self.height().get() as usize {
                let crop_text = self.grid[row][column].crop()
                    .map(|crop| crop.to_string())
                    .unwrap_or("-".to_string());

                writeln!(
                    text,
                    "{}",
                    crop_text,
                ).unwrap();
            }
        }

        text
    }

    pub fn deserialize(text: &str) -> Grid {
        let lines: Vec<&str> = text.split('\n').collect();
        let dims: Vec<_> = lines[0].split(' ').collect();
        let width = NonZeroU8::new(dims[0].parse().unwrap())
            .expect("Width must be positive.");
        let height = NonZeroU8::new(dims[1].parse().unwrap())
            .expect("Height must be positive.");

        let mut grid = Grid::new(width, height);
        for i in 0..lines.len()-1 {
            let line = lines[i+1];
            let crop_placement: Vec<_> = line.split(' ').collect();
            let name = crop_placement[0];
            if name != "-" {
                let crop_state = CropState {
                    crop: Crop::from_str(name)
                        .unwrap_or_else(|_| panic!("No crop named {} exists.", name)),
                    status: GrowingStatus::Growing
                };

                grid.grid[i % height.get() as usize][i / height.get() as usize]
                    .set_crop_state(crop_state);
            };
        }

        grid
    }

    fn crops(&self) -> Vec<Crop> {
        let mut crops = Vec::new();
        for column in 0..self.width().get() {
            for row in 0..self.height().get() {
                if let Ok(square) = self.at(Location {column, row}) {
                    if let Some(crop) = square.crop() {
                        crops.push(crop);
                    }
                }
            }
        }

        crops
    }

    fn square_at_mut(&mut self, location: Location) -> &mut Square {
        &mut self.grid[location.row as usize][location.column as usize]
    }

    fn expand(&mut self, width: u8, height: u8, soil: Soil) {
        self.add_columns(width, soil);
        self.add_rows(height, soil);
    }

    fn add_columns(&mut self, count: u8, soil: Soil) {
        let squares = vec![Square::empty(soil); count as usize];
        for i in 0..self.width().get() as usize {
            self.grid[i].append(&mut squares.clone());
        }
    }

    fn add_rows(&mut self, count: u8, soil: Soil) {
        let square = Square::empty(soil);
        for _ in 0..count {
            self.grid.push(vec![square; self.width().get() as usize]);
        }
    }
}
