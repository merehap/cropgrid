#[derive(Clone, Copy, Debug)]
pub struct Location {
    pub column: u8,
    pub row: u8,
}