use datetime::LocalDate;

use crate::grid::crop::Crop;
use crate::grid::placement::Placement;
use crate::grid::square::GrowingStatus;

#[derive(Clone)]
pub struct Interval {
    pub crop: Crop,
    pub planting: LocalDate,
    pub harvesting: LocalDate,
    pub placement: Placement,
}

impl Interval {
    pub fn growing_status_on_date(&self, date: &LocalDate) -> Option<GrowingStatus> {
        if *date == self.planting {
            Some(GrowingStatus::Planting)
        } else if *date == self.harvesting {
            Some(GrowingStatus::Harvesting)
        } else if *date > self.planting && *date < self.harvesting {
            Some(GrowingStatus::Growing)
        } else {
            None
        }
    }
}
