use num_derive::FromPrimitive;

use crate::grid::square::PH;
use crate::strum::IntoEnumIterator;

#[derive(Clone, Copy, Hash, PartialEq, Debug, Display, EnumIter, EnumString, FromPrimitive)]
pub enum Crop {
    Hazelnut,
    Almond,
    CornusMas,
    MountainAsh,
    Gingko,
    Chestnut,
    AutumnOlive,
    PawPaw,
    Apple,
    Pear,
    Plum,
    Pluot,
    Cherry,
    Medlar,
    Persimmon,
    Aprium,
    Nectarine,
    Pluerry,
    Shipova,
    Mulberry,
    Jujube,
    Walnut,
    NankingCherry,
    Hawthorn,
    Luma,
    HighbushCranberry,
    CrabApple,
    Apricot,
    JapaneseApricot,
    Peach,
    StrawberryTree,
    Fig,
    Quince,
    FloweringQuince,
    Nectaplum,
    PeachPlum,
    CherryPlum,
    HimalayanHoneysuckle,
    Seaberry,
    Goji,
    Honeyberry,
    Aronia,
    GoldenLantern,
    Jostaberry,
    Orus8,
    Kinnickinnick,
    Salal,
    Salmonberry,
    Serviceberry,
    Thimbleberry,
    Raspberry,
    Currant,
    Blackberry,
    Strawberry,
    Gooseberry,
    Blueberry,
    Wheat,
    Potato,
    Tomato,
    Corn,
    Bean,
    SoyBean,
    Oca,
    Carrot,
    Turnip,
    Cabbage,
    Collard,
    Kale,
    Peanut,
    SweetPotato,
    Squash,
    Oat,
    Millet,
    Barley,
    Rice,
    Rye,
    Teff,
    Quinoa,
    Sorghum,
    Cassava,
    Chickpea,
    Lentil,
    Pea,
    JerusalemArtichoke,
    AmericanGroundnut,
    PrairieTurnip,
    Parsnip,
    Radish,
    Ginger,
    Onion,
    GreenOnion,
    Garlic,
    Fennel,
    Turmeric,
    Yacon,
    BellPepper,
    HotPepper,
    Herbs,
    Pumpkin,
    Cantalope,
    Watermelon,
    Brocolli,
    Cucumber,
    Zuccini,
    Lettuce,
    SwissChard,
    Spinach,
    NapaCabbage,
    Cilantro,
    CowPea,
    Dill,
    Amaranth,
    Melon,
    Borage,
    Rutabaga,
    Gourd,
    Okra,
    Beat,
    MalabarSpinach,
}

impl Crop {
    pub fn all() -> Vec<Crop> {
        // TODO: Do this just once at startup.
        let mut all: Vec<_> = Crop::iter().collect();
        all.sort_by(|x, y| x.to_string().cmp(&y.to_string()));
        all
    }

    pub fn crop_info(self) -> CropInfo {
        use Crop::*;
        match self {
            Hazelnut => CropInfo {
                calories: 0,
                low_ph: PH::new(7.0),
                high_ph: PH::new(7.0),
            },
            Almond => CropInfo {
                calories: 0,
                low_ph: PH::new(7.0),
                high_ph: PH::new(7.0),
            },
            CornusMas => CropInfo {
                calories: 0,
                low_ph: PH::new(7.0),
                high_ph: PH::new(7.0),
            },
            MountainAsh => CropInfo {
                calories: 0,
                low_ph: PH::new(7.0),
                high_ph: PH::new(7.0),
            },
            Gingko => CropInfo {
                calories: 0,
                low_ph: PH::new(7.0),
                high_ph: PH::new(7.0),
            },
            Chestnut => CropInfo {
                calories: 0,
                low_ph: PH::new(7.0),
                high_ph: PH::new(7.0),
            },
            AutumnOlive => CropInfo {
                calories: 0,
                low_ph: PH::new(7.0),
                high_ph: PH::new(7.0),
            },
            PawPaw => CropInfo {
                calories: 0,
                low_ph: PH::new(7.0),
                high_ph: PH::new(7.0),
            },
            Apple => CropInfo {
                calories: 0,
                low_ph: PH::new(7.0),
                high_ph: PH::new(7.0),
            },
            Pear => CropInfo {
                calories: 0,
                low_ph: PH::new(7.0),
                high_ph: PH::new(7.0),
            },
            Plum => CropInfo {
                calories: 0,
                low_ph: PH::new(7.0),
                high_ph: PH::new(7.0),
            },
            Pluot => CropInfo {
                calories: 0,
                low_ph: PH::new(7.0),
                high_ph: PH::new(7.0),
            },
            Cherry => CropInfo {
                calories: 0,
                low_ph: PH::new(7.0),
                high_ph: PH::new(7.0),
            },
            Medlar => CropInfo {
                calories: 0,
                low_ph: PH::new(7.0),
                high_ph: PH::new(7.0),
            },
            Persimmon => CropInfo {
                calories: 0,
                low_ph: PH::new(7.0),
                high_ph: PH::new(7.0),
            },
            Aprium => CropInfo {
                calories: 0,
                low_ph: PH::new(7.0),
                high_ph: PH::new(7.0),
            },
            Nectarine => CropInfo {
                calories: 0,
                low_ph: PH::new(7.0),
                high_ph: PH::new(7.0),
            },
            Pluerry => CropInfo {
                calories: 0,
                low_ph: PH::new(7.0),
                high_ph: PH::new(7.0),
            },
            Shipova => CropInfo {
                calories: 0,
                low_ph: PH::new(7.0),
                high_ph: PH::new(7.0),
            },
            Mulberry => CropInfo {
                calories: 0,
                low_ph: PH::new(7.0),
                high_ph: PH::new(7.0),
            },
            Jujube => CropInfo {
                calories: 0,
                low_ph: PH::new(7.0),
                high_ph: PH::new(7.0),
            },
            Walnut => CropInfo {
                calories: 0,
                low_ph: PH::new(7.0),
                high_ph: PH::new(7.0),
            },
            NankingCherry => CropInfo {
                calories: 0,
                low_ph: PH::new(7.0),
                high_ph: PH::new(7.0),
            },
            Hawthorn => CropInfo {
                calories: 0,
                low_ph: PH::new(7.0),
                high_ph: PH::new(7.0),
            },
            Luma => CropInfo {
                calories: 0,
                low_ph: PH::new(7.0),
                high_ph: PH::new(7.0),
            },
            HighbushCranberry => CropInfo {
                calories: 0,
                low_ph: PH::new(7.0),
                high_ph: PH::new(7.0),
            },
            CrabApple => CropInfo {
                calories: 0,
                low_ph: PH::new(7.0),
                high_ph: PH::new(7.0),
            },
            Apricot => CropInfo {
                calories: 0,
                low_ph: PH::new(7.0),
                high_ph: PH::new(7.0),
            },
            JapaneseApricot => CropInfo {
                calories: 0,
                low_ph: PH::new(7.0),
                high_ph: PH::new(7.0),
            },
            Peach => CropInfo {
                calories: 0,
                low_ph: PH::new(7.0),
                high_ph: PH::new(7.0),
            },
            StrawberryTree => CropInfo {
                calories: 0,
                low_ph: PH::new(7.0),
                high_ph: PH::new(7.0),
            },
            Fig => CropInfo {
                calories: 0,
                low_ph: PH::new(7.0),
                high_ph: PH::new(7.0),
            },
            Quince => CropInfo {
                calories: 0,
                low_ph: PH::new(7.0),
                high_ph: PH::new(7.0),
            },
            FloweringQuince => CropInfo {
                calories: 0,
                low_ph: PH::new(7.0),
                high_ph: PH::new(7.0),
            },
            Nectaplum => CropInfo {
                calories: 0,
                low_ph: PH::new(7.0),
                high_ph: PH::new(7.0),
            },
            PeachPlum => CropInfo {
                calories: 0,
                low_ph: PH::new(7.0),
                high_ph: PH::new(7.0),
            },
            CherryPlum => CropInfo {
                calories: 0,
                low_ph: PH::new(7.0),
                high_ph: PH::new(7.0),
            },
            HimalayanHoneysuckle => CropInfo {
                calories: 0,
                low_ph: PH::new(7.0),
                high_ph: PH::new(7.0),
            },
            Seaberry => CropInfo {
                calories: 0,
                low_ph: PH::new(7.0),
                high_ph: PH::new(7.0),
            },
            Goji => CropInfo {
                calories: 0,
                low_ph: PH::new(7.0),
                high_ph: PH::new(7.0),
            },
            Honeyberry => CropInfo {
                calories: 0,
                low_ph: PH::new(7.0),
                high_ph: PH::new(7.0),
            },
            Aronia => CropInfo {
                calories: 0,
                low_ph: PH::new(7.0),
                high_ph: PH::new(7.0),
            },
            GoldenLantern => CropInfo {
                calories: 0,
                low_ph: PH::new(7.0),
                high_ph: PH::new(7.0),
            },
            Jostaberry => CropInfo {
                calories: 0,
                low_ph: PH::new(7.0),
                high_ph: PH::new(7.0),
            },
            Orus8 => CropInfo {
                calories: 0,
                low_ph: PH::new(7.0),
                high_ph: PH::new(7.0),
            },
            Kinnickinnick => CropInfo {
                calories: 0,
                low_ph: PH::new(7.0),
                high_ph: PH::new(7.0),
            },
            Salal => CropInfo {
                calories: 0,
                low_ph: PH::new(7.0),
                high_ph: PH::new(7.0),
            },
            Salmonberry => CropInfo {
                calories: 0,
                low_ph: PH::new(7.0),
                high_ph: PH::new(7.0),
            },
            Serviceberry => CropInfo {
                calories: 0,
                low_ph: PH::new(7.0),
                high_ph: PH::new(7.0),
            },
            Thimbleberry => CropInfo {
                calories: 0,
                low_ph: PH::new(7.0),
                high_ph: PH::new(7.0),
            },
            Raspberry => CropInfo {
                calories: 0,
                low_ph: PH::new(7.0),
                high_ph: PH::new(7.0),
            },
            Currant => CropInfo {
                calories: 0,
                low_ph: PH::new(7.0),
                high_ph: PH::new(7.0),
            },
            Blackberry => CropInfo {
                calories: 0,
                low_ph: PH::new(7.0),
                high_ph: PH::new(7.0),
            },
            Strawberry => CropInfo {
                calories: 0,
                low_ph: PH::new(7.0),
                high_ph: PH::new(7.0),
            },
            Gooseberry => CropInfo {
                calories: 0,
                low_ph: PH::new(7.0),
                high_ph: PH::new(7.0),
            },
            Blueberry => CropInfo {
                calories: 0,
                low_ph: PH::new(7.0),
                high_ph: PH::new(7.0),
            },
            Wheat => CropInfo {
                // https://www.motherearthnews.com/organic-gardening/~/media/295a54f778854c39b455f7b7db4f4c82.ashx
                calories: 89,
                // https://www.gardenguides.com/129938-soil-growing-wheat.html

                low_ph: PH::new(5.0),
                high_ph: PH::new(6.0),
            },
            Potato => CropInfo {
                // https://www.motherearthnews.com/organic-gardening/~/media/295a54f778854c39b455f7b7db4f4c82.ashx
                calories: 270,
                // https://growgreatpotatoes.com/2010/soil-ph-for-potatoes/
                // https://www.thespruce.com/growing-potatoes-in-the-home-garden-1403476
                low_ph: PH::new(5.0),
                high_ph: PH::new(6.0),
            },
            Tomato => CropInfo {
                calories: 0,
                // https://garden.org/learn/articles/view/358/
                low_ph: PH::new(6.0),
                high_ph: PH::new(6.8),
            },
            Corn => CropInfo {
                // https://www.motherearthnews.com/organic-gardening/~/media/295a54f778854c39b455f7b7db4f4c82.ashx
                calories: 300,
                // https://www.no-dig-vegetablegarden.com/growing-sweet-corn.html
                low_ph: PH::new(5.8),
                high_ph: PH::new(6.8),
            },
            Bean => CropInfo {
                // https://www.motherearthnews.com/organic-gardening/~/media/295a54f778854c39b455f7b7db4f4c82.ashx
                calories: 60,
                low_ph: PH::new(6.0),
                high_ph: PH::new(7.0),
            },
            SoyBean => CropInfo {
                calories: 0,
                low_ph: PH::new(6.0),
                high_ph: PH::new(7.0),
            },
            Strawberry => CropInfo {
                calories: 0,
                low_ph: PH::new(5.8),
                high_ph: PH::new(6.2),
            },
            Oca => CropInfo {
                calories: 0,
                low_ph: PH::new(5.7),
                high_ph: PH::new(7.5),
            },
            Carrot => CropInfo {
                calories: 0,
                low_ph: PH::new(6.0),
                high_ph: PH::new(6.8),
            },
            Turnip => CropInfo {
                calories: 0,
                // https://www.motherearthnews.com/organic-gardening/vegetables/growing-turnips-zm0z13aszkin
                low_ph: PH::new(6.0),
                high_ph: PH::new(7.0),
            },
            Cabbage => CropInfo {
                // https://www.motherearthnews.com/organic-gardening/~/media/295a54f778854c39b455f7b7db4f4c82.ashx
                calories: 100,
                // https://www.thespruce.com/growing-and-caring-for-cabbage-plants-1402815
                low_ph: PH::new(6.8),
                high_ph: PH::new(7.0),
            },
            Collard => CropInfo {
                // https://www.motherearthnews.com/organic-gardening/~/media/295a54f778854c39b455f7b7db4f4c82.ashx
                calories: 50,
                low_ph: PH::new(6.5),
                high_ph: PH::new(6.8),
            },
            Kale => CropInfo {
                // https://www.motherearthnews.com/organic-gardening/~/media/295a54f778854c39b455f7b7db4f4c82.ashx
                calories: 36,
                low_ph: PH::new(6.5),
                high_ph: PH::new(6.8),
            },
            Peanut => CropInfo {
                // https://www.motherearthnews.com/organic-gardening/~/media/295a54f778854c39b455f7b7db4f4c82.ashx
                calories: 180,
                low_ph: PH::new(5.9),
                high_ph: PH::new(6.3),
            },
            SweetPotato => CropInfo {
                // https://www.motherearthnews.com/organic-gardening/~/media/295a54f778854c39b455f7b7db4f4c82.ashx
                calories: 200,
                low_ph: PH::new(5.8),
                high_ph: PH::new(6.2),
            },
            Squash => CropInfo {
                // https://www.motherearthnews.com/organic-gardening/~/media/295a54f778854c39b455f7b7db4f4c82.ashx
                calories: 100,
                low_ph: PH::new(6.0),
                high_ph: PH::new(6.5),
            },
            Oat => CropInfo {
                calories: 0,
                low_ph: PH::new(4.5),
                high_ph: PH::new(6.0),
            },
            Millet => CropInfo {
                calories: 0,
                low_ph: PH::new(5.0),
                high_ph: PH::new(6.5),
            },
            Barley => CropInfo {
                calories: 0,
                low_ph: PH::new(6.0),
                high_ph: PH::new(8.5),
            },
            Rice => CropInfo {
                calories: 0,
                low_ph: PH::new(6.0),
                high_ph: PH::new(7.0),
            },
            Rye => CropInfo {
                calories: 0,
                low_ph: PH::new(5.5),
                high_ph: PH::new(7.0),
            },
            Teff => CropInfo {
                calories: 0,
                // https://www.farmanddairy.com/columns/implementing-teff-grass-into-your-forage-system/501003.html
                low_ph: PH::new(6.0),
                high_ph: PH::new(6.5),
            },
            Quinoa => CropInfo {
                calories: 0,
                // https://www.herbazest.com/herb-garden/growing-quinoa
                // LOTS OF CONFLICTING INFO!
                low_ph: PH::new(4.8),
                high_ph: PH::new(8.5),
            },
            Sorghum => CropInfo {
                calories: 0,
                low_ph: PH::new(6.0),
                high_ph: PH::new(6.5),
            },
            Cassava => CropInfo {
                calories: 0,
                low_ph: PH::new(5.5),
                high_ph: PH::new(6.5),
            },
            Chickpea => CropInfo {
                calories: 0,
                // https://gardeningwithsoule.com/you-can-grow-garbanzo/
                // CONFLICTING SOURCES
                low_ph: PH::new(6.5),
                high_ph: PH::new(7.2),
            },
            Lentil => CropInfo {
                calories: 0,
                low_ph: PH::new(6.0),
                high_ph: PH::new(8.0),
            },
            Pea => CropInfo {
                calories: 0,
                low_ph: PH::new(6.0),
                high_ph: PH::new(7.0),
            },
            JerusalemArtichoke => CropInfo {
                calories: 0,
                // https://www.motherearthnews.com/organic-gardening/vegetables/growing-jerusalem-artichokes-zmaz10onzraw
                // CONFLICTING SOURCES!
                low_ph: PH::new(6.5),
                high_ph: PH::new(7.5),
            },
            // Harvest after two seasons for bigger size.
            AmericanGroundnut => CropInfo {
                calories: 0,
                low_ph: PH::new(5.1),
                high_ph: PH::new(7.0),
            },
            PrairieTurnip => CropInfo {
                calories: 0,
                low_ph: PH::new(6.5),
                high_ph: PH::new(7.5),
            },
            Parsnip => CropInfo {
                calories: 0,
                low_ph: PH::new(6.6),
                high_ph: PH::new(7.2),
            },
            Radish => CropInfo {
                calories: 0,
                low_ph: PH::new(6.5),
                high_ph: PH::new(7.0),
            },
            Ginger => CropInfo {
                calories: 0,
                low_ph: PH::new(5.5),
                high_ph: PH::new(6.5),
            },
            Onion => CropInfo {
                calories: 0,
                low_ph: PH::new(6.0),
                high_ph: PH::new(7.0),
            },
            GreenOnion => CropInfo {
                calories: 0,
                low_ph: PH::new(6.2),
                high_ph: PH::new(6.8),
            },
            Garlic => CropInfo {
                calories: 0,
                low_ph: PH::new(6.5),
                high_ph: PH::new(7.0),
            },
            Fennel => CropInfo {
                calories: 0,
                low_ph: PH::new(5.5),
                high_ph: PH::new(6.8),
            },
            Turmeric => CropInfo {
                calories: 0,
                low_ph: PH::new(6.0),
                high_ph: PH::new(7.8),
            },
            Yacon => CropInfo {
                calories: 0,
                low_ph: PH::new(6.0),
                high_ph: PH::new(6.5),
            },
            BellPepper => CropInfo {
                calories: 0,
                low_ph: PH::new(6.0),
                high_ph: PH::new(6.8),
            },
            HotPepper => CropInfo {
                calories: 0,
                low_ph: PH::new(6.0),
                high_ph: PH::new(6.8),
            },
            Herbs => CropInfo {
                calories: 0,
                low_ph: PH::new(6.0),
                high_ph: PH::new(7.0),
            },
            Pumpkin => CropInfo {
                calories: 0,
                low_ph: PH::new(6.5),
                high_ph: PH::new(7.8),
            },
            Cantalope => CropInfo {
                calories: 0,
                low_ph: PH::new(6.5),
                high_ph: PH::new(7.5),
            },
            Watermelon => CropInfo {
                calories: 0,
                low_ph: PH::new(6.0),
                high_ph: PH::new(6.8),
            },
            Brocolli => CropInfo {
                calories: 0,
                low_ph: PH::new(6.5),
                high_ph: PH::new(7.0),
            },
            Cucumber => CropInfo {
                calories: 0,
                low_ph: PH::new(6.0),
                high_ph: PH::new(6.8),
            },
            Zuccini => CropInfo {
                calories: 0,
                low_ph: PH::new(6.0),
                high_ph: PH::new(6.8),
            },
            Lettuce => CropInfo {
                calories: 0,
                low_ph: PH::new(6.0),
                high_ph: PH::new(7.0),
            },
            SwissChard => CropInfo {
                calories: 0,
                low_ph: PH::new(6.2),
                high_ph: PH::new(6.8),
            },
            Spinach => CropInfo {
                calories: 0,
                low_ph: PH::new(6.5),
                high_ph: PH::new(7.5),
            },
            NapaCabbage => CropInfo {
                calories: 0,
                low_ph: PH::new(6.5),
                high_ph: PH::new(7.0),
            },
            Cilantro => CropInfo {
                calories: 0,
                low_ph: PH::new(6.5),
                high_ph: PH::new(7.5),
            },
            CowPea => CropInfo {
                calories: 0,
                low_ph: PH::new(7.0),
                high_ph: PH::new(7.0),
            },
            Dill => CropInfo {
                calories: 0,
                low_ph: PH::new(7.0),
                high_ph: PH::new(7.0),
            },
            Amaranth => CropInfo {
                calories: 0,
                low_ph: PH::new(7.0),
                high_ph: PH::new(7.0),
            },
            Melon => CropInfo {
                calories: 0,
                low_ph: PH::new(7.0),
                high_ph: PH::new(7.0),
            },
            Borage => CropInfo {
                calories: 0,
                low_ph: PH::new(7.0),
                high_ph: PH::new(7.0),
            },
            Rutabaga => CropInfo {
                calories: 0,
                low_ph: PH::new(7.0),
                high_ph: PH::new(7.0),
            },
            Gourd => CropInfo {
                calories: 0,
                low_ph: PH::new(7.0),
                high_ph: PH::new(7.0),
            },
            Okra => CropInfo {
                calories: 0,
                low_ph: PH::new(7.0),
                high_ph: PH::new(7.0),
            },
            Beat => CropInfo {
                calories: 0,
                low_ph: PH::new(7.0),
                high_ph: PH::new(7.0),
            },
            MalabarSpinach => CropInfo {
                calories: 0,
                low_ph: PH::new(7.0),
                high_ph: PH::new(7.0),
            },
        }
    }
}

// Per square foot.
pub struct CropInfo {
    pub calories: usize,
    pub low_ph: PH,
    pub high_ph: PH,
}
