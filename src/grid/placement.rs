use std::num::NonZeroU8;

use crate::grid::location::Location;

#[derive(Clone)]
pub struct Placement {
    pub left_column: u8,
    pub width: NonZeroU8,
    pub top_row: u8,
    pub height: NonZeroU8,
}

impl Placement {
    pub fn upper_left(&self) -> Location {
        Location {
            column: self.left_column,
            row: self.top_row,
        }
    }

    pub fn bottom_right(&self) -> Location {
        Location {
            column: self.left_column + self.width.get() - 1,
            row: self.top_row + self.height.get() - 1,
        }
    }

    pub fn locations(&self) -> Vec<Location> {
        let mut locations = Vec::new();
        let right_column = self.left_column + self.width.get();
        for column in self.left_column..right_column {
            let bottom_row = self.top_row + self.height.get();
            for row in self.top_row..bottom_row {
                locations.push(Location {column, row});
            }
        }

        locations
    }

    pub fn area(&self) -> u16 {
        self.width.get() as u16 * self.height.get() as u16
    }

    pub fn is_in_bounds(&self, location: Location) -> bool {
        let column = location.column;
        let row = location.row;
        column >= self.left_column &&
            column < self.left_column + self.width.get() &&
            row >= self.top_row &&
            row < self.top_row + self.height.get()
    }
}
